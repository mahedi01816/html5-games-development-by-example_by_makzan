var websocketGame = {
    isDrawing: false,
    startX: 0,
    startY: 0,
    LINE_SEGMENT: 0,
    CHAT_MESSAGE: 1,
    GAME_LOGIC: 2,
    WAITING_TO_START: 0,
    GAME_START: 1,
    GAME_OVER: 2,
    GAME_RESTART: 3,
    isTurnToDraw: false
};
var canvas = document.getElementById("drawing-pad");
var ctx = canvas.getContext("2d");

$(function () {
    if (window["WebSocket"]) {
        websocketGame.socket = new WebSocket("");
        websocketGame.socket.onopen = function (ev) {
            console.log("connection ok");
        };
        websocketGame.socket.onmessage = function (ev) {
            console.log("onmessage event : ", e.data);
            var data = JSON.parse(ev.data);
            if (data.dataType == websocketGame.CHAT_MESSAGE) {
                $("#chat-history").append("<li>" + data.sender + " said : " + data.message + "</li>");
            } else if (data.dataType == websocketGame.LINE_SEGMENT) {
                drawLine(ctx, data.startX, data.startY, data.endX, data.endY, 1);
            } else if (data.dataType == websocketGame.GAME_LOGIC) {
                if (data.gameState == websocketGame.GAME_OVER) {
                    websocketGame.isTurnToDraw = false;
                    $("#chat-history").append("<li>" + data.winner + " wins! The answer is " + data.answer + "</li>");
                    $("#restart").show();
                }
                if (data.gameState == websocketGame.GAME_START) {
                    canvas.width = canvas.width;
                    $("#restart").hide();
                    $("#chat-history").html("");
                    if (data.isPlayerTurn) {
                        isTurnToDraw = true;
                        $("#chat-history").append("<li>Your turn to draw " + data.answer + "</li>");
                    } else {
                        $("#chat-history").append("<li>Game started</li>");
                    }
                }
            }
            $("#chat-history").append("<li>" + ev.data + "</li>");
            console.log(ev.data);
        };
        websocketGame.socket.onclose = function (ev) {
            console.log('close');
        };
    }
});

$("#send").click(sendMessage);

$("#chat-input").keypress(function (event) {
    if (event.keyCode == '13') {
        sendMessage();
    }
});

function sendMessage() {
    var message = $("#chat-input").val();
    var data = {};
    data.dataType = websocketGame.CHAT_MESSAGE;
    data.message = message;
    websocketGame.socket.send(JSON.stringify(data));
    $("#chat-input").val("");
}

$("#drawing-pad").mousedown(function (e) {
    var mouseX = e.layerX || 0;
    var mouseY = e.layerY || 0;
    startX = mouseX;
    startY = mouseY;
    isDrawing = true;
});

$("#drawing-pad").mousemove(function (e) {
    if (websocketGame.isDrawing) {
        var mouseX = e.layerX || 0;
        var mouseY = e.layerY || 0;

        if (!(mouseX == websocketGame.startX && mouseY == websocketGame.startY)) {
            drawLine(ctx, startX, startY, mouseX, mouseY, 1);
            var data = {};
            data.dataType = websocketGame.LINE_SEGMENT;
            data.startX = startX;
            data.startY = startY;
            data.endX = mouseX;
            data.endY = mouseY;
            websocketGame.socket.send(JSON.stringify(data));
            websocketGame.startX = mouseX;
            websocketGame.startY = mouseY;
        }
    }
});

$("#drawing-pad").mouseup(function (e) {
    websocketGame.isDrawing = false;
});

function drawLine(ctx, x1, y1, x2, y2, thickness) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineWidth = thickness;
    ctx.strokeStyle = "#444";
    ctx.stroke();
}