var audioGame = {};

audioGame.musicNotes = [];
audioGame.levelData = "1.592, 3; 1.984, 2; 2.466, 1; 2.949, 2; 4.022, 3;";
audioGame.dots = [];
audioGame.startingTime = 0;
audioGame.dotImage = new Image();
audioGame.totalDotsCount = 0;
audioGame.totalSucessCount = 0;
audioGame.sucessCount = 5;
audioGame.isRecordMode = true;

$(function () {
    audioGame.melody = document.getElementById("melody");
    audioGame.base = document.getElementById("base");
    audioGame.dotImage.src = "images/dot.png";

    audioGame.buttonOverSound = document.getElementById("button-over");
    audioGame.buttonOverSound.volume = 0.3;
    audioGame.buttonActiveSound = document.getElementById("button-active");
    audioGame.buttonActiveSound.volume = 0.3;

    $("a[href='#game']")
        .hover(function () {
                audioGame.buttonOverSound.currentTime = 0;
                audioGame.buttonOverSound.play();
            },
            function () {
                audioGame.buttonOverSound.pause();
            })
        .click(function () {
            audioGame.buttonActiveSound.currentTime = 0;
            audioGame.buttonActiveSound.play();

            $("#game-scene").addClass('show-scene');
            startGame();

            return false;
        });

    $(document).keydown(function (e) {
        var line = e.which - 73;
        $('#hit-line-' + line).removeClass('hide');
        $('#hit-line-' + line).addClass('show');

        if (audioGame.isRecordMode) {
            if (e.which == 186) {
                var musicNotesString = "";
                for (var i in audioGame.musicNotes) {
                    musicNotesString += audioGame.musicNotes[i].time + ", " + audioGame.musicNotes[i].line + ";";
                }
                console.log(musicNotesString);
            }
            var currentTime = parseInt(audioGame.melody.currentTime * 1000) / 1000;
            var note = new MusicNote(currentTime, e.which - 73);
            audioGame.musicNotes.push(note);
        } else {
            var hitLine = e.which - 73;

            for (var i in audioGame.dots) {
                if (hitLine == audioGame.dots[i].line && Math.abs(audioGame.dots[i].distance) < 20) {
                    audioGame.dots.splice(i, 1);
                    audioGame.sucessCount++;
                    audioGame.sucessCount = Math.min(5, audioGame.sucessCount);
                    audioGame.totalSucessCount++;
                }
            }
        }
        if (!audioGame.isRecordMode) {
            setupLevelData();
            setInterval(gameLoop, 30);
        }
    });

    $(document).keyup(function (e) {
        var line = e.which - 73;
        $('#hit-line-' + line).removeClass('show');
        $('#hit-line-' + line).addClass('hide');
    });

    $(audioGame.melody).bind('ended', onMelodyEnded);

    drawBackground();
    setupLevelData();
    setInterval(gameLoop, 30);
});

function drawBackground() {
    var game = document.getElementById("game-background-canvas");
    var ctx = game.getContext("2d");

    ctx.lineWidth = 10;
    ctx.strokeStyle = "#000";

    var center = game.width / 2;

    ctx.beginPath();
    ctx.moveTo(center - 100, 50);
    ctx.lineTo(center - 100, ctx.canvas.height - 50);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(center, 50);
    ctx.lineTo(center, ctx.canvas.height - 50);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(center + 100, 50);
    ctx.lineTo(center + 100, ctx.canvas.height - 50);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(center - 150, ctx.canvas.height - 80);
    ctx.lineTo(center + 150, ctx.canvas.height - 80);
    ctx.lineWidth = 1;
    ctx.strokeStyle = "rgba(50, 50, 50, 0.8)";
    ctx.stroke();
}

function MusicNote(time, line) {
    this.time = time;
    this.line = line;
}

function Dot(distance, line) {
    this.distance = distance;
    this.line = line;
    this.missed = false;
}

function setupLevelData() {
    var notes = audioGame.levelData.split(";");

    audioGame.totalDotsCount = notes.length;

    for (var i in notes) {
        var note = notes[i].split(",");
        var time = parseFloat(note[0]);
        var line = parseInt(note[1]);
        var musicNote = new MusicNote(time, line);
        audioGame.musicNotes.push(musicNote);
    }
}

function startGame() {
    var date = new Date();

    audioGame.startingTime = date.getTime();
    setTimeout(playMusic, 3550);
}

function playMusic() {
    audioGame.melody.play();
    audioGame.base.play();
}

function gameLoop() {
    var game = document.getElementById("game-canvas");
    var ctx = game.getContext('2d');

    var successPercent = audioGame.sucessCount / 5;
    successPercent = Math.max(0, Math.min(1, successPercent));
    audioGame.melody.volume = successPercent;

    if (audioGame.startingTime != 0) {
        for (var i in audioGame.musicNotes) {
            var date = new Date();
            var elapsedTime = (date.getTime() - audioGame.startingTime) / 1000;
            var note = audioGame.musicNotes[i];
            var timeDifference = note.time - elapsedTime;

            if (timeDifference >= 0 && timeDifference <= 0.03) {
                var dot = new Dot(ctx.canvas.height - 150, note.line);
                audioGame.dots.push(dot);
            }
        }
    }

    for (var i in audioGame.dots) {
        audioGame.dots[i].distance -= 2.5;
    }

    ctx.clearRect(ctx.canvas.width / 2 - 200, 0, 400, ctx.canvas.height);

    for (var i in audioGame.dots) {
        var circle_gradient = ctx.createRadialGradient(-3, -3, 1, 0, 0, 20);

        circle_gradient.addColorStop(0, "#fff");
        circle_gradient.addColorStop(1, "cc0");
        ctx.fillStyle = circle_gradient;

        ctx.save();

        var center = game.width / 2;
        var dot = audioGame.dots[i];
        var x = center - 100;

        if (dot.line == 2) {
            x = center;
        } else if (dot.line == 3) {
            x = center + 100;
        }

        ctx.translate(x, ctx.canvas.height - 80 - audioGame.dots[i].distance);
        ctx.drawImage(audioGame.dotImage, -audioGame.dotImage.width / 2, -audioGame.dotImage.height / 2);
        ctx.restore();

        if (!audioGame.dots[i].missed && audioGame.dots[i].distance < -10) {
            audioGame.dots[i].missed = true;
            audioGame.sucessCount--;
            audioGame.sucessCount = Math.max(0, audioGame.sucessCount);
        }

        if (audioGame.dots[i].distance < -100) {
            audioGame.dots.splice(i, 1);
        }
    }
}

function onMelodyEnded() {
    console.log('song ended');
    console.log('success percent : ', audioGame.totalSucessCount / audioGame.totalDotsCount * 100 + '%');
}